import numpy as np
from mpyll import parallelize

def count_in_circle_points(data, r, m):
    a = np.array(data) # matrix, each row contains a point coordinates
    d = np.sqrt(np.sum(a ** 2, axis = 1)) # distance to the origin
    in_circle = d <= r # a numpy array, True if distance <= radius, False otherwise
    return np.sum(in_circle)

def estimate_pi(data, m):
    pi_estimation = 4 * np.sum(data) / m
    return pi_estimation

def test_parallelize():
    r = 1.
    m = 10 ** 6
    X = np.random.uniform(-r, r, size = m)
    Y = np.random.uniform(-r, r, size = m)
    data = [(X[i], Y[i]) for i in range(m)]
    rv = parallelize(task = count_in_circle_points, 
                     data = data, data_shuffle = False, 
                     post_processor = estimate_pi, 
                     n_jobs = -1, 
                     # task arguments
                     count_in_circle_points_r = r, 
                     count_in_circle_points_m = m,
                     # post processor arguments
                     estimate_pi_m = m)
    assert np.abs(rv - np.pi) < 0.01
